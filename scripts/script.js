$(document).ready(function () {
  solidifyNavbar();
  showTab();

  $(window).scroll(solidifyNavbar);
  $('.accordion__element').click(toggleAccordion);

  $('.product__carousel').slick({
    slidesToShow: 1,
  });
  var verticalValue = window.innerWidth < 992 ? false : true;
  $('.product__miniature').slick({
    slidesToShow: 4,
    arrows: false,
    asNavFor: '.product__carousel',
    vertical: verticalValue,
    focusOnSelect: true,
  });

  $('#input-number__minus').click(decrementInput);
  $('#input-number__plus').click(incrementInput);

  $('.input input, .input textarea').focusin(function() {
    $(this).parent().addClass('focused');
  });
  
  $('.input input, .input textarea').focusout(function() {
    $(this).parent().removeClass('focused');
  });

  $('.tabs__titles li').click(function() {
    $('.tabs__titles .active').removeClass('active');
    $(this).addClass('active');
    showTab();
  });
});

function solidifyNavbar() {
  var scrollPos = $(window).scrollTop();
  if (scrollPos > 10) {
    $('.navbar').addClass("navbar--solid");
  } else {
    $('.navbar').removeClass("navbar--solid");
  }
}

function toggleAccordion() {
  $(this).parent().children('.accordion--open').toggleClass('accordion--open');
  $(this).toggleClass('accordion--open');
}

function incrementInput() {
  var value = parseInt($(this).parent().children('input').val());
    $(this).parent().children('input').val(value+1);
}

function decrementInput() {
  var value = $(this).parent().children('input').val();
  if(value > 0) {
    $(this).parent().children('input').val(value-1);
  }
}

function showTab() {
  var $selectedTab = $('.tabs__titles .active').attr('id');
  var tabContentId = '#' + $selectedTab + '-content';
  $('.tabs__content').hide();
  $(tabContentId).show();
}